<?php
namespace TestTask\BannerManager\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class BannerManager extends Template
{

    protected $scopeConfig;
    protected $collectionFactory;
    protected $objectManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \TestTask\BannerManager\Model\ResourceModel\Bannermanager\CollectionFactory $collectionFactory,
        ObjectManagerInterface $objectManager
    ) {

        $this->scopeConfig = $context->getScopeConfig();
        $this->collectionFactory = $collectionFactory;
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }


    public function getActiveBanners()
    {

        $collection = $this->collectionFactory->create()->addFieldToFilter('isactive', 1);

        if ($ids_list = $this->getBannerBlockArguments()) {
            $collection->addFilter('banner_id', ['in' => $ids_list], 'public');
        }


        return $collection;
    }


    public function getBannerBlockArguments()
    {

        $list =  $this->getBannerList();
        $listArray = [];

        if ($list != '') {
            $listArray = explode(',', $list);
        }

        return $listArray;
    }
}
