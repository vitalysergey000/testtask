<?php

namespace TestTask\BannerManager\Model;

class Bannermanager extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('TestTask\BannerManager\Model\ResourceModel\Bannermanager');
    }
}
