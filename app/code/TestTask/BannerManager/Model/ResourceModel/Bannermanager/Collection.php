<?php

namespace TestTask\BannerManager\Model\ResourceModel\Bannermanager;

use \TestTask\BannerManager\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('TestTask\BannerManager\Model\Bannermanager', 'TestTask\BannerManager\Model\ResourceModel\Bannermanager');
        $this->_map['fields']['banner_id'] = 'main_table.banner_id';
    }
}
