<?php

namespace TestTask\BannerManager\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Bannermanager extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('banner_slider', 'banner_id');
    }
}
