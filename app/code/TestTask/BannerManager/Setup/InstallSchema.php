<?php

namespace TestTask\BannerManager\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $table = $setup->getConnection()
            ->newTable($setup->getTable('banner_slider'))
            ->addColumn(
                'banner_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Banner ID'
            )
            ->addColumn(
                'isactive',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                ['default' => true],
                'is Active'
            )
            ->addColumn(
                'title',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'title'
            )
            ->addColumn(
                'text_banner',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'URL'
            )
            ->addColumn(
                'text_popap',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Banner Image'
            )
            ->addColumn(
                'date_start',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                255,
                ['nullable' => false],
                'URL'
            )
            ->addColumn(
                'date_end',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                255,
                ['nullable' => true],
                'URL'
            )
            ->addColumn(
                'rewiew_once',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                255,
                ['nullable' => false, 'default' => 0],
                'URL'
            )->setComment("Banner Slider table");
        $setup->getConnection()->createTable($table);
    }
}
